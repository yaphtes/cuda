#include <iostream>
#include <node.h>
#include <cmath>
#include <chrono>

extern double integrateGPU_cuda(double a, double b, size_t n, size_t nt);

namespace addon {
  using namespace node;
  using namespace v8;

  using v8::FunctionCallbackInfo;
  using v8::Isolate;
  using v8::Local;
  using v8::Number;
  using v8::Object;
  using v8::String;
  using v8::Value;

  inline double f(double x) {
    return std::sin(x);
  }

  void integrateCPU(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    
    double a = args[0]->NumberValue();
    double b = args[1]->NumberValue();
    double n = args[2]->NumberValue();
    
    double x = a;
    double sum = 0;
    double h = (b - a) / n;
    
    for (int i = 0; i < n; ++i) {
      sum += h * (f(x) + f(x + h)) / 2;
      x += h;
    }

    args.GetReturnValue().Set(Number::New(isolate, sum));
  }

  void integrateGPU(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    
    double a = args[0]->NumberValue();
    double b = args[1]->NumberValue();
    size_t n = args[2]->NumberValue();
    size_t nt = args[3]->NumberValue();
    
    if (n % nt != 0) {
      // Throw an Error that is passed back to JavaScript
      isolate->ThrowException(Exception::Error(
        String::NewFromUtf8(isolate, "Количество шагов не кратно количеству нитей")));
      return;
    }

    double sum = integrateGPU_cuda(a, b, n, nt);
    args.GetReturnValue().Set(Number::New(isolate, sum));
  }


  void Init(Local<Object> exports) {
    NODE_SET_METHOD(exports, "integrateCPU", integrateCPU);
    NODE_SET_METHOD(exports, "integrateGPU", integrateGPU);
    
  }

  NODE_MODULE(NODE_GYP_MODULE_NAME, Init)
}