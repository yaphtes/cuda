function f(x) { //Подынтегральная функция
  return Math.sin(x);
}

module.exports = function intagrate(a, b, n) {
  let x = a;
  let result = 0;
  let h = (b - a) / n;

  for (let i = 0; i < n; i++) {
    result += h * (f(x) + f(x + h)) / 2;
    x += h;
  }

  return result;
};