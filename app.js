const colors = require('colors');
const MAX_BLOCKS = 65535; // Максимальное количество блоков в CUDA

const addon = require('./build/Release/addon.node');
const nodeSingleThread = require('./integral.js');

class Integral {
  constructor(a, b, n) {
    this.a = a;
    this.b = b;
    this.n = n;
    this.nt = 512; // количество нитей
  }

  multiThreads() {
    this.multiCuda();
  }

  singleThread() {
    this.singleNode();
    this.singleC();
  }

  singleNode() {
    let { a, b, n } = this;

    let start = Date.now();
    let result = nodeSingleThread(a, b, n);
    let time = Date.now() - start;
    
    this.logResult('Node', result, time);
  }

  singleC() {
    let { a, b, n } = this;

    let start = Date.now();
    let result = addon.integrateCPU(a, b, n);
    let time = Date.now() - start;

    this.logResult('C++', result, time);
  }

  multiCuda() {
    let { a, b, n, nt } = this;

    if (n % nt != 0) {
      console.warn(colors.yellow(`Количество шагов дополнено до кратного количества нитей: ${nt}`).bgBlack);
      n += nt - (n % nt);
    }

    if (n / nt > MAX_BLOCKS) {
      console.warn(colors.yellow(`Количество блоков усечено до макс: ${MAX_BLOCKS}`).bgBlack);
      n = MAX_BLOCKS * nt;
    }

    console.log(colors.bold.blue(`Всего потоков CUDA: ${MAX_BLOCKS * nt}`).bgBlack);
    let start = Date.now();
    let result = addon.integrateGPU(a, b, n, nt);
    let time = Date.now() - start;

    this.logResult('Cuda', result, time);
  }

  logResult(str, result, time) {
    console.log(`${colors.bold(str + ' result:')} ${colors.green(result)}`.white.bgBlack);
    console.log(`${colors.bold(str + ' time:')} ${colors.green(time + 'ms')}\n`.white.bgBlack);
  }
}

let integral = new Integral(0, 2, 1e8);
integral.singleThread();
integral.multiThreads();