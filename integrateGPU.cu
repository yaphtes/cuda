#include <cuda_runtime.h>
#include <stdio.h>
#include <math.h>

extern double integrateGPU_cuda(double a, double b, size_t n, size_t nt);

// __device__ - функция может наботать на GPU
__device__ inline double f(double x) {
  return sin(x);
}

// __global__ - вызываем с CPU, считаем на GPU
__global__ void kernel(double a, double b, double h, double *dA) {
  size_t idx = blockIdx.x * blockDim.x + threadIdx.x; //задаем идентификатор нити
  double x = a + h * idx;
  dA[idx] = h * (f(x) + f(x + h)) / 2; // метод прямоугольников
}

double integrateGPU_cuda(double a, double b, size_t n, size_t nt) {
  double *hA; //указатель на область памяти хоста
  double *dA; //указатель на область памяти устройства

  hA = (double*)malloc(n * sizeof(double)); //выделяем память на хосте
  cudaMalloc((void**)&dA, n * sizeof(double)); //выделяем память на устройстве    

  double sum = 0;
  double h = (b - a) / n;

  kernel <<< n / nt, nt >>> (a, b, h, dA);
  
  //передаем данные на хост
  cudaMemcpy(hA, dA, n * sizeof(double), cudaMemcpyDeviceToHost);
  for (size_t i = 0; i < n; i++) {
    sum += hA[i];
  }

  free(hA);
  cudaFree(dA);

  return sum;
}