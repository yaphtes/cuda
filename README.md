### Что?
В данном репозитории, реализован рассчет интеграла методом прямоугольников.

### Как?
Написан C++ addon для Node.js. Этот addon имеет доступ к cuda, который впоследствии реализует параллельный рассчет.

### Технологии:
* [Node.js][node]
* [C++ addons][addons]
* [CUDA][cuda]

### Как запустить:

* `npm install`
* `npm start`


### У вас должно быть установлено:

* [CUDA Toolkits][cuda] && [Подробное руководство по установке][cuda-install] (проверить командой `nvcc --version`)
* [Node.js][node-install] (проверить командой `node --version`)
* gcc, g++ компиляторы версии 6-




[node]: https://nodejs.org
[addons]: https://nodejs.org/dist/latest-v9.x/docs/api/addons.html#addons_c_addons
[cuda]: https://developer.nvidia.com/cuda-downloads
[cuda-install]: https://developer.download.nvidia.com/compute/cuda/9.1/Prod/docs/sidebar/CUDA_Quick_Start_Guide.pdf
[node-install]: https://nodejs.org/en/download/
