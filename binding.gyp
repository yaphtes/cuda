{
  "targets": [
    {
      "target_name": "addon",
      "sources": ["integrateCPU.cc", "integrateGPU.cu"],
      'rules': [{
        'extension': 'cu',           
        'inputs': ['<(RULE_INPUT_PATH)'],
        'outputs':[ '<(INTERMEDIATE_DIR)/<(RULE_INPUT_ROOT).o'],
        'conditions': [
          [ 'OS=="win"',  
            {
              'rule_name': 'cuda on windows',
              'message': "compile cuda file on windows",
              'process_outputs_as_sources': 0,
              'action': ['nvcc -c <(_inputs) -o  <(_outputs)'],
            }, 
            {
              'rule_name': 'cuda on linux',
              'message': "compile cuda file on linux",
              'process_outputs_as_sources': 1,
              'action': ['nvcc','-Xcompiler','-fpic','-c', '<@(_inputs)','-o','<@(_outputs)'],
            }
          ]
        ]
      }],

      'conditions': [
        ['OS=="linux"', {
          'libraries': ['-lcuda', '-lcudart'],
          'include_dirs': ['/usr/local/include'],
          'library_dirs': ['/usr/local/lib', '/usr/local/cuda/lib64'],
        }],
      ]
    }
  ]
}




